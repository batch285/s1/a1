package com.zuitt.activity;

import java.util.Scanner;
public class activity {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;

        System.out.print("Enter your first name: ");
        firstName = scanner.nextLine();

        System.out.print("Enter your last name: ");
        lastName = scanner.nextLine();

        System.out.print("Enter your grade for the first subject: ");
        firstSubject = Double.parseDouble(scanner.nextLine());

        System.out.print("Enter your grade for the second subject: ");
        secondSubject = Double.parseDouble(scanner.nextLine());

        System.out.print("Enter your grade for the third subject: ");
        thirdSubject = Double.parseDouble(scanner.nextLine());

        double average = (firstSubject + secondSubject + thirdSubject) / 3;
        String formattedAverage = String.format("%.2f", average);

        System.out.println("Good day, " + firstName + " " + lastName +". \n" + "Your grade average is: " + formattedAverage);
    }
}
